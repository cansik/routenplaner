﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
	public class WayPoint
	{
		public string Name { get; set; }

		public double Longitude { get; set; }

		public double Latitude { get; set; }

		public WayPoint (string _name, double _latitude, double _longitude)
		{ 
			Name = _name; 
			Latitude = _latitude; 
			Longitude = _longitude; 
		}

		public override string ToString ()
		{
			var sb = new StringBuilder ();
			sb.Append ("WayPoint: ");

			if (Name != null && !String.IsNullOrEmpty (Name))
				sb.Append (Name + " ");

			sb.Append (Latitude.ToString ("F2", CultureInfo.InvariantCulture) + "/" + Longitude.ToString ("F2", CultureInfo.InvariantCulture));

			return sb.ToString ();
		}

		public double Distance (WayPoint targtet)
		{
			var RADIUS = 6371;
			var TO_RAD = Math.PI / 180;
			var lat1 = targtet.Latitude * TO_RAD;
			var lat2 = Latitude * TO_RAD;
			var long1 = targtet.Longitude * TO_RAD;
			var long2 = Longitude * TO_RAD;

			return RADIUS * Math.Acos (Math.Sin (lat1) * Math.Sin (lat2) + Math.Cos (lat1) * Math.Cos (lat2) * Math.Cos (long2 - long1));
		}

		public static WayPoint operator + (WayPoint c1, WayPoint c2)
		{
			return new WayPoint (c1.Name, c1.Latitude + c1.Latitude, c1.Longitude + c2.Longitude);
		}

		public static WayPoint operator - (WayPoint c1, WayPoint c2)
		{
			return new WayPoint (c1.Name, c1.Latitude - c1.Latitude, c1.Longitude - c2.Longitude);
		}
	}
}
