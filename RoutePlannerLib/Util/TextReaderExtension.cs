﻿using System;
using System.Linq;
using System.IO;
using System.Xml.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Util
{
	public static class TextReaderExtension
	{
		public static IEnumerable<String[]> GetSplittedLines (this TextReader reader, char delimiter)
		{
			//very short linq solution, but not with yield
			//return reader.ReadToEnd ().Split (Environment.NewLine.ToCharArray ()).Select (l => l.Split (delimiter));

			string line;
			while ((line = reader.ReadLine ()) != null) {
				yield return line.Split (delimiter);
			}

		}
	}
}

