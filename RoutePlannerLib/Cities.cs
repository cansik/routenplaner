﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;
using Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Util;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
	public class Cities
	{
      
		public List<City> AllCities { get; set; }

		public Cities ()
		{
			AllCities = new List<City> ();
		}

		public int Count {
			get {
				return AllCities.Count;
			}
		}

		public City this [int index] {
			get {
				if (AllCities.Count <= index)
					return null;
				else
					return AllCities [index];
			}
			set {
				AllCities [index] = value;
			}
		}

		public int ReadCities (string filename)
		{
			int readCities = 0;
			using (var reader = new StreamReader (filename)) {

				var splittedText = reader.GetSplittedLines ('\t');
				foreach (var row in splittedText) {
					AllCities.Add (new City () {
						Name = row [0],
						Country = row [1],
						Population = Convert.ToInt32 (row [2]),
						Location = new WayPoint (row [0], double.Parse (row [3], CultureInfo.InvariantCulture), double.Parse (row [4], CultureInfo.InvariantCulture))
					});
					readCities++;
				}

			}

			return readCities;
		}

		public List<City> FindNeighbours (WayPoint location, double distance)
		{
			return AllCities.Where (c => c.Location.Distance (location) <= distance).ToList ();
		}

		/*  public City FindCity(string cityName)
        {
            return FindCityByName(delegate(City c)
            {
                return c.Name == cityName.Trim();
            });
        }
        
        public City FindCityByName(Predicate<City> _pred)
        {
            foreach (var c in AllCities)
                if (_pred(c))
                    return c;
            return null;
        }*/

		public City FindCity (string cityName)
		{
			return AllCities.Find (c => string.Compare (c.Name, cityName, true) == 0);
		}

		#region Lab04: FindShortestPath helper function

		/// <summary>
		/// Find all cities between 2 cities 
		/// </summary>
		/// <param name="from">source city</param>
		/// <param name="to">target city</param>
		/// <returns>list of cities</returns>
		public List<City> FindCitiesBetween (City from, City to)
		{
			var foundCities = new List<City> ();
			if (from == null || to == null)
				return foundCities;

			foundCities.Add (from);

			var minLat = Math.Min (from.Location.Latitude, to.Location.Latitude);
			var maxLat = Math.Max (from.Location.Latitude, to.Location.Latitude);
			var minLon = Math.Min (from.Location.Longitude, to.Location.Longitude);
			var maxLon = Math.Max (from.Location.Longitude, to.Location.Longitude);

			// rename the name of the "cities" variable to your name of the internal City-List
			foundCities.AddRange (AllCities.FindAll (c =>
				c.Location.Latitude > minLat && c.Location.Latitude < maxLat
			&& c.Location.Longitude > minLon && c.Location.Longitude < maxLon));

			foundCities.Add (to);
			return foundCities;
		}

		#endregion
	}
}
