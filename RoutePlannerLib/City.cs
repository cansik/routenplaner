﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
	public class City
	{
		public string Name { get; set; }

		public string Country { get; set; }

		public int Population { get; set; }

		public WayPoint Location { get; set; }

		public City ()
		{

		}

		public City (string _name, string _country, int _population, double _latidute, double _longitude)
		{
			Name = _name;
			Country = _country;
			Population = _population;
			Location = new WayPoint (_name, _latidute, _longitude);
		}

        
		public override bool Equals (object obj)
		{
			var city = obj as City;

			if (city == null)
				return false;

			return Name == city.Name && Country == city.Country;
		}


		public override int GetHashCode ()
		{
			return (Name + Country).GetHashCode ();
		}

		public static bool operator == (City a, City b)
		{
			// If both are null, or both are same instance, return true.
			if (System.Object.ReferenceEquals (a, b)) {
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)a == null) || ((object)b == null)) {
				return false;
			}

			// Return true if the fields match:
			return a.Equals (b);
		}

		public static bool operator != (City a, City b)
		{
			return !(a == b);
		}

	}
}
