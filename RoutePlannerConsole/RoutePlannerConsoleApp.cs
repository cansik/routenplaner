﻿using System;
using System.Reflection;
using Fhnw.Ecnf.RoutePlanner.RoutePlannerLib;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerConsole
{
	class RoutePlannerConsoleApp
	{
		private const string CitiesTestFile = "citiesTestDataLab2.txt";

		static void Main (string[] args)
		{
			Console.WriteLine ("Welcome to RoutePlaner (Version {0})", Assembly.GetExecutingAssembly ().GetName ().Version);
            
			//Lab01 Test 2d
			var wayPoint = new WayPoint ("Windisch", 47.479319847061966, 8.212966918945312);
			var wayPoint1 = new WayPoint ("", 47.479319847061966, 8.212966918945312); 

			Console.WriteLine ("{0}: {1}/{2}", wayPoint.Name, wayPoint.Latitude, wayPoint.Longitude);

			//Lab 02 Test 1a
			Console.WriteLine ("{0}", wayPoint.ToString ());
			Console.WriteLine ("{0}", wayPoint1.ToString ());

			var cities = new Cities ();
			cities.ReadCities (CitiesTestFile);
			cities.ReadCities (CitiesTestFile);

			Console.ReadLine ();
		}
	}
}
